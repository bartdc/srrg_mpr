#include <iomanip>
#include <dirent.h> //to folder navigation
#include "srrg_image_utils/depth_utils.h"
#include "srrg_messages/pinhole_image_message.h"
#include "srrg_messages/spherical_image_message.h"
#include "srrg_mpr/mpr_pyramid_generator.h"
#include "srrg_mpr/mpr_solver.h"
#include "srrg_mpr/mpr_tracker.h"
#include "srrg_mpr/mpr_utils.h"
#include "srrg_system_utils/system_utils.h"
#include "srrg_types/cloud_3d.h"

// uncomment for timing stats
#define __PROFILE__

using namespace std;
using namespace srrg_core;
using namespace srrg_mpr;

typedef std::map<
    std::string,
    MPRTracker::Config,
    std::less<std::string>,
    Eigen::aligned_allocator<std::pair<std::string, MPRTracker::Config> > >
    StringMPRTrackerConfigMap;

#include <math.h>
// VELODYNE64 has 2deg of positive rays and and 24.9deg of negative.
// this var serves as a row shifting
const float VELODYNE_HFOV = 360.0*M_PI/180.0;
const float VELODYNE_VFOV = 26.9*M_PI/180.0;
const float VELODYNE_POSITIVE_RAYS = 2.0 * M_PI / 180.0;
const float POSITIVE_RAYS = 2.0*M_PI/180.0;
const float VELODYNE_IMAGE_CENTER_SHIFT = 11.45 * M_PI / 180.0;
// i.e. (26.9/2)-2, (velodyne fov/2 - positive rays)
//
Eigen::Isometry3f T_mpr2Kitti, T_mpr2Kitti_inv;

StringMPRTrackerConfigMap configs;

bool sortByName(const std::string& file1, const std::string& file2) {
  return file1 < file2;
}

std::vector<std::string> listDirectoryFiles(const std::string& folder_path) {
  std::vector<std::string> file_list; 
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (folder_path.c_str())) != NULL) {
    /* print all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      if(!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
        continue;
      //printf ("%s\n", ent->d_name);
      file_list.push_back(ent->d_name);      
    }
    closedir (dir);
  } else {
    /* could not open directory */
    std::runtime_error("unable to list files in the selected directory");
  }
  sort(file_list.begin(), file_list.end(), sortByName);
  return file_list;
}

//! from here: https://github.com/yanii/kitti-pcl/blob/master/KITTI_README.TXT
void readVelodyneBin(Cloud3D& velodyne_cloud, const std::string& binary_file) {
  velodyne_cloud.clear();
  // load point cloud
  FILE *stream;
  stream = fopen(binary_file.c_str(),"rb");
  if(!stream)
    cerr << "problem in opening file!\n"; 
  // allocate 4 MB buffer (only ~130*4*4 KB are needed)
  int32_t num = 1000000;
  float *data = (float*)malloc(num*sizeof(float));
  // pointers
  float *px = data+0;
  float *py = data+1;
  float *pz = data+2;
  float *pr = data+3;
  num = fread(data,sizeof(float),num,stream)/4;  
  for (int32_t i=0; i<num; i++) {
    RichPoint3D point(Eigen::Vector3f(*px, *py, *pz));
    velodyne_cloud.push_back(point);
    px+=4; py+=4; pz+=4; pr+=4;
  }  
  fclose(stream);
  free(data); 
}

struct VelodyneParams{
  int rows;
  int cols;
  float min_depth;
  float max_depth;
  float hres;
  float vres;
  float normals_max_endpoint_distance;
  int normals_cross_region_range_col;
  int normals_cross_region_range_row;
  int normals_blur_region_size;
  void printParams(){
    printf("IMG image_dim: [%d x %d]   depth_range: [%f - %f]   res h-v: [%f - %f]\nPYR normal_max_endpt_dist: [%f]  normal_cross_reg_range_col: [%d] normal_cross_reg_range_row: [%d]  normal_blur_reg_size: [%d]\n",rows,cols,min_depth,max_depth,hres,vres,normals_max_endpoint_distance, normals_cross_region_range_col, normals_cross_region_range_row, normals_blur_region_size);
  }
};

Eigen::Isometry3f interpolate(const double& relative_time_,
                              const Eigen::Isometry3f& zero_time_isometry_,
                              const Eigen::Isometry3f& one_time_isometry_) {
  if(relative_time_ < 0.0 || relative_time_ > 1.0)
    throw std::runtime_error("[interpolate]: specify a time in the interval (0,1)");
    
  Eigen::Matrix<float,7,1> zero_time = srrg_core::t2w(zero_time_isometry_);
  Eigen::Quaternionf zero_q(zero_time(6), zero_time(3), zero_time(4), zero_time(5));
  Eigen::Matrix<float,7,1> one_time  = srrg_core::t2w(one_time_isometry_);
  Eigen::Quaternionf one_q(one_time(6), one_time(3), one_time(4), one_time(5));
  //interpolate quaternion
  zero_q.slerp(relative_time_, one_q);
  zero_q.normalize();
      
  //lerp
  Eigen::Matrix<float,6,1> result = Eigen::Matrix<float,6,1>::Zero();
  result.head(3) = zero_time.head(3) * (1.0 - relative_time_);
  result.head(3) += relative_time_ * one_time.head(3);

  result(3) = zero_q.x();
  result(4) = zero_q.y();
  result(5) = zero_q.z();

  if(zero_q.w() < 0.0) 
    result.tail(3) *= -1;

  return srrg_core::v2t(result);    
}

void projectVelodyneCloud(FloatImage& depth_image,
                          const VelodyneParams& velodyne_params,
                          const Cloud3D& velodyne_cloud,
                          const Eigen::Isometry3f& motion_model_isometry) {
  const int& rows = velodyne_params.rows;
  const int& cols = velodyne_params.cols;
  const float& min_depth = velodyne_params.min_depth;
  const float& max_depth = velodyne_params.max_depth;
  const float& hres = velodyne_params.hres;
  const float& vres = velodyne_params.vres;
  
  Eigen::Matrix3f camera_matrix;
  convertCameraMatrix(camera_matrix,Eigen::Vector4f(0.f,0.f,-hres,-vres),POSITIVE_RAYS*vres,cols);
  Eigen::Vector3f camera_point = Eigen::Vector3f::Zero();
  Eigen::Vector2f image_point = Eigen::Vector2f::Zero();
  
  depth_image.create(rows, cols);
  depth_image = 0;

  for(size_t i=0; i < velodyne_cloud.size(); ++i) {
    const RichPoint3D& full_point = velodyne_cloud[i];

    const float relative_time = (float)i/(float)velodyne_cloud.size();
    const Eigen::Isometry3f interpolated_T = interpolate(relative_time,
                                                         Eigen::Isometry3f::Identity(),
                                                         motion_model_isometry);
    const Eigen::Vector3f interpolated_point = interpolated_T.inverse() * full_point.point();
    
    float depth = interpolated_point.norm();
    if(depth < min_depth || depth > max_depth)
      continue;

    camera_point.x() = atan2(interpolated_point.y(), interpolated_point.x());
    camera_point.y() = atan2(interpolated_point.z(), interpolated_point.head<2>().norm());
    camera_point.z() = depth;

    image_point.x()= camera_matrix(0,0)*camera_point.x() + camera_matrix(0,2);
    image_point.y()= camera_matrix(1,1)*camera_point.y() + camera_matrix(1,2);
    
    //int irow = -cvRound(image_point.y()) + rows/2;
    //int icol = -cvRound(image_point.x()) + cols;
    
    int irow = cvRound(image_point.y());
    int icol = cvRound(image_point.x());
    if(irow < 0 || irow > rows || icol < 0 || icol > cols) {
      continue;
    }

    depth_image.at<float>(irow, icol) = depth; 
  }

}


void initConfigs() {
  MPRTracker::Config c_velodyne64;
  c_velodyne64.pyramid.camera_type = MPRPyramidLevel::Spherical;
  c_velodyne64.pyramid.normals_max_endpoint_distance = 15.f;
  c_velodyne64.pyramid.normals_cross_region_range_col = 1;
  c_velodyne64.pyramid.normals_cross_region_range_row = 2;
  c_velodyne64.pyramid.normals_scaled_blur_multiplier = 0;
  c_velodyne64.pyramid.normals_blur_region_size = 2;
  c_velodyne64.pyramid.min_depth = 1.5f;
  c_velodyne64.pyramid.max_depth = 120.f;
  c_velodyne64.pyramid.scales.push_back(std::make_pair(2, 2));
  c_velodyne64.levels_iterations.push_back(50);
  c_velodyne64.solver.omega_intensity = 0.;  // default no intensity
  c_velodyne64.solver.depth_error_rejection_threshold = 10.f;
  c_velodyne64.solver.kernel_chi_threshold = .1;
  configs.insert(std::make_pair("velodyne64", c_velodyne64));
}

const char* banner[] = {
  "\n\nUsage: rosrun srrg_mpr kitti_velodyne_tracker <bin_folder>"
  "\n",
  "Options:\n", "------------------------------------------\n",
  "-eval         <string>   output dump file with odometry in evaluation format",
  "-show-images  <flag>     show processed images",
  "-h                       this help\n",
  0
};


int main(int argc, char** argv) {

  bool evaluate = false;
  bool verbose = false;
  bool use_motion_model = true;
  bool show_images = false;

  bool time_stats = false;
  std::string time_stats_filename;

  float omega_intensity = 0.f;
  float omega_depth = 1.f;
  float omega_normals = 1.f;

  std::ofstream evaluation_file;
  
  // velodyne params
  float keyframe_trans = 5.f;
  float keyframe_rot = 0.1f;

  std::string config_name = "velodyne64";
  initConfigs();

  std::string folder_path = "";
  std::string evaluation_filename = "";
  
  int c = 1;
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-eval")) {
      c++;
      evaluation_filename = argv[c];
      evaluate = true;
    } else if (!strcmp(argv[c], "-show-images")) {
      show_images = true;
    } else {
      folder_path = argv[c];
    }
    c++;
  }

  std::vector<std::string> file_list = listDirectoryFiles(folder_path);
  // current image index
  const int cloud_number = file_list.size();
  
  VelodyneParams velodyne_params;
  velodyne_params.rows = 64;
  velodyne_params.cols = 870;
  velodyne_params.min_depth = 1.5;
  velodyne_params.max_depth = 120; //VELODYNE_64
  velodyne_params.hres = velodyne_params.cols/VELODYNE_HFOV;
  velodyne_params.vres = velodyne_params.rows/VELODYNE_VFOV;
  velodyne_params.normals_max_endpoint_distance = 1.f;
  velodyne_params.normals_cross_region_range_col = 2;
  velodyne_params.normals_cross_region_range_row = 2;
  velodyne_params.normals_blur_region_size = 2;
  
  // Axis rotation for kitti evaluation
  Eigen::Matrix3f R;
  // calib matrix from KITTI dataset (calib.txt)
  R << -1.857739385241e-03, -9.999659513510e-01, -8.039975204516e-03,
      -6.481465826011e-03, 8.051860151134e-03, -9.999466081774e-01,
      9.999773098287e-01, -1.805528627661e-03, -6.496203536139e-03;

  T_mpr2Kitti.setIdentity();
  T_mpr2Kitti.linear() = R;
  T_mpr2Kitti.translation() = Eigen::Vector3f(
      -4.784029760483e-03, -7.337429464231e-02, -3.339968064433e-01);
  T_mpr2Kitti_inv = T_mpr2Kitti.inverse();

  Eigen::Matrix3f image_center_correction = Eigen::Matrix3f(
      Eigen::AngleAxisf(VELODYNE_IMAGE_CENTER_SHIFT, Eigen::Vector3f::UnitZ()));
  Eigen::Isometry3f T_velodyne_image_center_shift;
  T_velodyne_image_center_shift.setIdentity();
  T_velodyne_image_center_shift.linear() = image_center_correction;

  Eigen::Isometry3f kitti_odom;
  kitti_odom.setIdentity();

  // store evaluation in TUM format
  if (evaluate) evaluation_file.open(evaluation_filename);

  MPRTracker tracker;
  tracker.setup();

  auto config_it = configs.find(config_name);
  if (config_it == configs.end()) {
    cerr << "config [" << config_name << "] not available" << endl;
    return 0;
  }

  std::vector<int> level_iterations;
  tracker.mutableConfig() = config_it->second;
  tracker.mutableConfig().solver.omega_intensity = omega_intensity;
  tracker.mutableConfig().solver.omega_depth = omega_depth;
  tracker.mutableConfig().solver.omega_normals = omega_normals;
  tracker.mutableConfig().keyframe_max_translation = keyframe_trans;
  tracker.mutableConfig().keyframe_max_rotation = keyframe_rot;
  tracker.mutableConfig().verbose = verbose;

  /// Stats
  int number_of_frames_current_window = 0;

  // The transform
  Eigen::Isometry3f odom;
  odom.setIdentity();

  RGBImage rgb_image;  
  FloatImage depth_image;

  int skip = 1;
  int count = 0;
  bool pyramid_init = false;

  double time_pyramid_cumulative = 0;
  double time_linearize_cumulative = 0;

  double time_projections_cumulative = 0;
  int max_dropped_frames = 3;

  int num_keyframes = 0;

  Eigen::Isometry3f previous_odom, motion_model_odom;
  previous_odom.setIdentity();
  motion_model_odom.setIdentity();

  std::vector<MPRStats, Eigen::aligned_allocator<MPRStats> > stats_history;
  int seq = -1;

  //camera matrix
  Eigen::Matrix3f K;
  K.setZero();
  convertCameraMatrix(K,Eigen::Vector4f(0.f,
                                        0.f,
                                        -velodyne_params.hres,
                                        -velodyne_params.vres),
                      POSITIVE_RAYS*velodyne_params.vres,
                      velodyne_params.cols);

  rgb_image.create(velodyne_params.rows, velodyne_params.cols);
  rgb_image = cv::Vec3b(0, 0, 0);

  Cloud3D velodyne_cloud;
  velodyne_cloud.clear();
  
  // main loop
  for(size_t cloud_id = 0; cloud_id < cloud_number; ++cloud_id) {
    
    double time_frame = getTime();

    if (!pyramid_init) {
      // set camera matrix, rows, cols
      const int rows = velodyne_params.rows;
      const int cols = velodyne_params.cols;
      cerr << "initializing pyramid: " << rows << "x" << cols << endl;
      tracker.mutableConfig().pyramid.rows = rows;
      tracker.mutableConfig().pyramid.cols = cols;
      tracker.mutableConfig().pyramid.camera_matrix = K;
      cerr << "rows: " << tracker.mutableConfig().pyramid.rows << endl;
      cerr << "cols: " << tracker.mutableConfig().pyramid.cols << endl;
      cerr << "K:    " << tracker.mutableConfig().pyramid.camera_matrix << endl;
      cerr << "pyramid initialized" << endl;
      pyramid_init = true;
    }

    // todo scan-egomotion for velodyne
    readVelodyneBin(velodyne_cloud, folder_path + "/"+file_list[cloud_id]);
    projectVelodyneCloud(depth_image, velodyne_params, velodyne_cloud, motion_model_odom);
    RawDepthImage raw_depth_image = depth_image * 1000.f; //m to mm

    tracker.setImages(raw_depth_image, rgb_image);

    // //from lower resolution to higher
    std::ostringstream line_stream;
    line_stream
        << "**********************************************************\n";
    line_stream << "FRAME: " << number_of_frames_current_window << endl;

    tracker.setRelativeMotionGuess(motion_model_odom);

    previous_odom = odom;
    Eigen::Isometry3f motion_model_old_odom = tracker.globalT();
    tracker.compute();
    odom = tracker.globalT();
    motion_model_odom = motion_model_old_odom.inverse() * odom;

    const MPRStats& stats = tracker.stats();
    if (stats.is_keyframe) { num_keyframes++; }
    line_stream << "keyframe num: " << num_keyframes << endl;

    for (int idx = stats.size() - 1; idx >= 0; --idx) {
      const MPRLevelStats& level_stats = stats[idx];
      const int num_iterations = level_stats.iterations.size();
      line_stream << "Level: "
                  << " { num: " << idx << ", "
                  << "iter: " << num_iterations << ", "
                  << "points: " << level_stats.cloud_size << ", ";
      const MPRIterationStats& start_iteration_stats =
          *level_stats.iterations.begin();
      const MPRIterationStats& end_iteration_stats =
          *level_stats.iterations.rbegin();
      line_stream << "chi: [" << start_iteration_stats.chi << ", "
                  << end_iteration_stats.chi << "], ";
      line_stream << "inliers: [" << start_iteration_stats.num_inliers << ", "
                  << end_iteration_stats.num_inliers << "] }\n";
    }
    line_stream << "Timings: { ";
    line_stream << "pyramid : " << stats.time_pyramid << ", ";
    line_stream << "project : " << stats.timeProjections() << ", ";
    line_stream << "linearize : " << stats.timeLinearize() << "}\n";

    time_pyramid_cumulative += stats.time_pyramid;
    time_projections_cumulative += stats.timeProjections();
    time_linearize_cumulative += stats.timeLinearize();

    line_stream << "**********************************************************"
                << endl;
    cerr << line_stream.str() << endl;
    stats_history.push_back(stats);

    ++number_of_frames_current_window;

    if (evaluate) {
      // first rotate the poses to be compliant with the reference frames (x
      // -> z and so on)
      // then apply the pitch rotation due to velodyne fov, then add the
      // relative motion
      kitti_odom =
        kitti_odom * (T_mpr2Kitti * motion_model_odom * T_mpr2Kitti_inv);
      Eigen::Isometry3f kitti_odom_image_shifted =
        T_velodyne_image_center_shift *
        kitti_odom;
      for (uint8_t r = 0; r < 3; ++r)
        for (uint8_t c = 0; c < 4; ++c)
          evaluation_file << kitti_odom_image_shifted(r, c) << " ";
      evaluation_file << endl;
      // std::cerr << "##########################################   z: "<<
      // kitti_odom(1,3) << std::endl;
    }

    if (show_images) {
      cv::imshow("Input depth/range", raw_depth_image);
      cv::waitKey(1);
    }

  }

#ifdef __PROFILE__
  float cumulative_time = time_linearize_cumulative +
                          time_projections_cumulative + time_pyramid_cumulative;
  float period = cumulative_time / (float)number_of_frames_current_window;
  std::cerr << "number of frames: " << number_of_frames_current_window << endl
            << endl;
  std::cerr << "cumulative pyramid time: " << time_pyramid_cumulative << endl;
  std::cerr << "cumulative projections time: " << time_projections_cumulative
            << endl;
  std::cerr << "cumulative linearize time: " << time_linearize_cumulative
            << endl;
  std::cerr << "cumulative time: " << cumulative_time << endl;
  std::cerr << "avg time per frame: " << period << " s" << endl;
  std::cerr << "frequency: " << 1. / period << " Hz" << endl;
#endif  // __PROFILE__

  return 0;
}
