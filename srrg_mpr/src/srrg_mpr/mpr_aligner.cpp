#include "mpr_aligner.h"
#include "srrg_system_utils/system_utils.h"

namespace srrg_mpr {
using srrg_core::getTime;
using namespace std;

MPRAligner::~MPRAligner() {
  if (_config_ptr != nullptr) delete _config_ptr;
}

void MPRAligner::setup() { _config_ptr = new Config; }

MPRAligner::Config& MPRAligner::mutableConfig() {
  if (_config_ptr == nullptr) {
    throw std::runtime_error("config invalid. Call setup() after construction");
  }
  _is_ready = false;
  return *_config_ptr;
}

const MPRAligner::Config& MPRAligner::constConfig() const {
  if (_config_ptr == nullptr) {
    throw std::runtime_error("config invalid. Call setup() after construction");
  }
  return *_config_ptr;
}

void MPRAligner::setFixedImages(const srrg_core::RawDepthImage& raw_depth,
                                const srrg_core::RGBImage& raw_rgb) {
  _depth_fixed_raw = raw_depth;
  _rgb_fixed_raw = raw_rgb;
}

void MPRAligner::setMovingImages(const srrg_core::RawDepthImage& raw_depth,
                                 const srrg_core::RGBImage& raw_rgb) {
  _depth_moving_raw = raw_depth;
  _rgb_moving_raw = raw_rgb;
}

void MPRAligner::init() {
  if (_is_ready) return;
  const int num_levels = numLevels();
  if (!numLevels()) { throw std::runtime_error("0 levels"); }

  if (constConfig().pyramid.scales.size() !=
      constConfig().levels_iterations.size()) {
    throw std::runtime_error(
        "error level of the pyramids and size of level iterations mismatch");
  }
  _pyramid_generator.mutableConfig() = mutableConfig().pyramid;

  cerr << "config: " << _pyramid_generator.mutableConfig().rows << " "
       << _pyramid_generator.mutableConfig().cols << endl;
  _solvers.resize(num_levels);
  _stats.resize(num_levels);
  for (int l = 0; l < num_levels; l++) {
    MPRLevelStats& level_stat = _stats[l];
    level_stat.iterations.resize(mutableConfig().levels_iterations[l]);
    _solvers[l].mutableConfig() = mutableConfig().solver;
  }

  // init hysteresis params
  _best_chi = FLT_MAX;
  _best_T = Eigen::Isometry3f::Identity();
  _higher_chi.resize(mutableConfig().hysteresis_size, false);

  _is_ready = true;
}

// constructs a pyramid in the generator, using raw_depth and raw rgb images
const MPRPyramid& MPRAligner::buildPyramid(
    const srrg_core::RawDepthImage& raw_depth,
    const srrg_core::RGBImage& raw_rgb) {
  _pyramid_generator.setImages(raw_depth, raw_rgb);
  _pyramid_generator.compute();
  return _pyramid_generator.pyramid();
}

// performs the alignment, of moving vs fixed using _T as initial guess
// result in _T
void MPRAligner::align(const MPRPyramid& fixed_pyramid,
                       const MPRPyramid& moving_pyramid) {
  const Config& conf = constConfig();

  double time_linearize = 0;
  double time_projections = 0;
  const int num_levels = conf.levels_iterations.size();
  std::vector<bool>::iterator hysteresis_it;

  for (int l = num_levels - 1; l >= 0; --l) {
    MPRSolver& solver = _solvers[l];
    MPRLevelStats& level_stats = _stats[l];
    const MPRPyramidLevel& pyr = fixed_pyramid[l];
    const Cloud3D& cloud = moving_pyramid[l]._cloud;
    level_stats.rows = pyr._rows;
    level_stats.cols = pyr._cols;
    level_stats.cloud_size = cloud.size();

    // termination criterion reset
    _best_chi = FLT_MAX;
    std::fill(_higher_chi.begin(), _higher_chi.end(), false);
    hysteresis_it = _higher_chi.begin();

    solver.setPyramidLevel(pyr);
    solver.setCloud(cloud);
    solver.setT(_T);
    const int level_iterations = conf.levels_iterations[l];
    for (int iteration = 0; iteration < level_iterations; ++iteration) {
      MPRIterationStats& iteration_stats = level_stats.iterations[iteration];
      solver.compute();
      iteration_stats.T = solver.T();
      iteration_stats.chi = solver.totalChi();
      iteration_stats.num_inliers = solver.numInliers();

      time_projections += solver.timeProjections();
      time_linearize += solver.timeLinearize();
      // termination criterion
      if (solver.totalChi() < _best_chi) {
        _best_chi = solver.totalChi();
        _best_T = solver.T();
        std::fill(_higher_chi.begin(), _higher_chi.end(), false);
      } else {
        *hysteresis_it = true;
        bool stop = false;
        for (size_t i = 0; i < conf.hysteresis_size; ++i)
          stop &= _higher_chi[i];
        if (stop) break;
        if (hysteresis_it == _higher_chi.end())
          hysteresis_it = _higher_chi.begin();
        else
          hysteresis_it++;
      }
    }
    level_stats.time_projections = time_projections;
    level_stats.time_linearize = time_linearize;

    _T = _best_T;  // solver.T();
  }
  _stats.T = _best_T;  ///_T;
}

void MPRAligner::compute() {
  using namespace std;
  init();
  // compute the pyramid of the moving image
  double time_pyramid = getTime();
  _moving_pyramid = buildPyramid(_depth_moving_raw, _rgb_moving_raw);
  _fixed_pyramid = buildPyramid(_depth_fixed_raw, _rgb_fixed_raw);
  time_pyramid = getTime() - time_pyramid;

  _stats.time_pyramid = time_pyramid;
  align(_fixed_pyramid, _moving_pyramid);
}
}
