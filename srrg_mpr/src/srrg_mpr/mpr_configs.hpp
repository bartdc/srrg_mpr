#pragma once

#include "srrg_mpr/mpr_tracker.h"

namespace srrg_mpr {
  
  typedef std::map<
    std::string,
    MPRTracker::Config,
    std::less<std::string>,
    Eigen::aligned_allocator<std::pair<std::string, MPRTracker::Config> > >
  StringMPRTrackerConfigMap;

  
  void initConfigs(StringMPRTrackerConfigMap& configs) {
    MPRTracker::Config c_base;

    c_base.pyramid.min_depth = .3;
    c_base.pyramid.max_depth = 10;

    MPRTracker::Config c_320x240 = c_base;
    c_320x240.pyramid.scales.push_back(std::make_pair(2, 2));
    c_320x240.levels_iterations.push_back(3);
    c_320x240.pyramid.scales.push_back(std::make_pair(4, 4));
    c_320x240.levels_iterations.push_back(30);
    configs.insert(std::make_pair("320x240", c_320x240));

    MPRTracker::Config c_640x480 = c_base;
    c_640x480.pyramid.scales.push_back(std::make_pair(8, 8));
    c_640x480.levels_iterations.push_back(20);
    c_640x480.pyramid.scales.push_back(std::make_pair(16, 16));
    c_640x480.levels_iterations.push_back(5);
    configs.insert(std::make_pair("640x480", c_640x480));

    MPRTracker::Config c_kinectv2 = c_base;
    c_kinectv2.pyramid.scales.push_back(std::make_pair(4, 4));
    c_kinectv2.levels_iterations.push_back(30);
    c_kinectv2.pyramid.scales.push_back(std::make_pair(8, 8));
    c_kinectv2.levels_iterations.push_back(30);
    c_kinectv2.solver.omega_intensity = 0.f;
    configs.insert(std::make_pair("kinectv2", c_kinectv2));

    MPRTracker::Config c_720x120_spherical;
    c_720x120_spherical.pyramid.camera_type = MPRPyramidLevel::Spherical;
    c_720x120_spherical.pyramid.normals_max_endpoint_distance = 1.f;
    c_720x120_spherical.pyramid.normals_cross_region_range_col = 2;
    c_720x120_spherical.pyramid.normals_cross_region_range_row = 2;
    c_720x120_spherical.pyramid.normals_blur_region_size = 2;
    c_720x120_spherical.pyramid.min_depth = 1.5f;
    c_720x120_spherical.pyramid.max_depth = 30.f;
    c_720x120_spherical.pyramid.scales.push_back(std::make_pair(2, 2));
    c_720x120_spherical.levels_iterations.push_back(100);
    c_720x120_spherical.solver.omega_intensity = 0.;  // default no intensity
    c_720x120_spherical.solver.depth_error_rejection_threshold = 3.f;
    c_720x120_spherical.solver.kernel_chi_threshold = .1;
    configs.insert(std::make_pair("720x120_spherical", c_720x120_spherical));

    MPRTracker::Config c_velodyne64;
    c_velodyne64.pyramid.camera_type = MPRPyramidLevel::Spherical;
    c_velodyne64.pyramid.normals_max_endpoint_distance = 15.f;
    c_velodyne64.pyramid.normals_cross_region_range_col = 1;
    c_velodyne64.pyramid.normals_cross_region_range_row = 2;
    c_velodyne64.pyramid.normals_scaled_blur_multiplier = 0;
    c_velodyne64.pyramid.normals_blur_region_size = 2;
    c_velodyne64.pyramid.min_depth = 1.5f;
    c_velodyne64.pyramid.max_depth = 120.f;
    c_velodyne64.pyramid.scales.push_back(std::make_pair(2, 2));
    c_velodyne64.levels_iterations.push_back(50);
    c_velodyne64.solver.omega_intensity = 0.;  // default no intensity
    c_velodyne64.solver.depth_error_rejection_threshold = 10.f;
    c_velodyne64.solver.kernel_chi_threshold = .1;
    configs.insert(std::make_pair("velodyne64", c_velodyne64));
  }

  
}
