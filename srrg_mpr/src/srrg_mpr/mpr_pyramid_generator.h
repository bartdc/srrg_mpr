#pragma once
#include "mpr_pyramid.h"
#include <iostream>
#include <srrg_types/cloud_3d.h>
#include <vector>

namespace srrg_mpr {

// MPRPyramidGenerator: generate the pyramidLevels
// it follows the init/set/compute paradigm
class MPRPyramidGenerator {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  enum FilterPolicy { Ignore, Suppress, Clamp };

  struct Config {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    enum FilterPolicy { Ignore, Suppress, Clamp };
    int rows = 0;
    int cols = 0;
    Eigen::Matrix3f camera_matrix = Eigen::Matrix3f::Identity();
    Eigen::Isometry3f sensor_offset = Eigen::Isometry3f::Identity();
    float min_depth = 0.3;
    float max_depth = 10.;
    int normals_cross_region_range_col = 3;
    int normals_cross_region_range_row = 3;
    int normals_scaled_blur_multiplier = 1;
    float normals_max_endpoint_distance = 0.3;
    int normals_blur_region_size = 3;
    float intensity_derivative_threshold = 10;
    float depth_derivative_threshold = 0.5;
    float normals_derivative_threshold = 0.1;
    FilterPolicy intensity_policy = Ignore;
    FilterPolicy depth_policy = Suppress;
    FilterPolicy normals_policy = Clamp;
    MPRPyramidLevel::CameraType camera_type = MPRPyramidLevel::Pinhole;
    std::vector<std::pair<int, int> > scales;  // scaling for rows and cols
  };

  MPRPyramidGenerator();
  ~MPRPyramidGenerator() {}

  // sets the images. Remember to call compute() afterwards
  void setImages(const srrg_core::RawDepthImage& raw_depth,
                 const srrg_core::RGBImage& raw_rgb);

  // get the pyramid vector calculated after compute()
  inline MPRPyramid& pyramid() { return _pyramid; }

  inline const Config& constConfig() const { return *_config; }
  inline Config& mutableConfig() {
    _containers_ready = false;
    return *_config;
  }

  // calculates the pyramid (stored in _pyramid)
  void compute();

 private:
  // prepares the storage
  void prepareContainers();

  // asserts if the images are compatible with config
  void checkImageSizes(const srrg_core::RawDepthImage& depth_image,
                       const srrg_core::RGBImage& rgb_image);

  // computes the derivatives and applies the filtering
  void computeDerivatives(MPRPyramidLevel& level);

  void makeCloud(MPRPyramidLevel& level, const Float3Image& direction_vectors);

  bool _containers_ready;
  Config* _config;
  MPRPyramid _pyramid;

  srrg_core::RawDepthImage _raw_depth;          // depth image (uint16)
  srrg_core::UnsignedCharImage _raw_intensity;  // intensity (0-255) (uint8)
  srrg_core::FloatImage _depth;                 // depth image (mm)
  srrg_core::FloatImage _intensity;             // grayscale image
  srrg_core::UnsignedCharImage _mask;           // invalid pixels in source
  Float3Image _normals;                         // normals of input image
  Float3Image _points;                          // 2D points of input image

  Float3Image _direction_vectors;  // image of the direction vectors of pixel
                                   // (to compute cloud)
  Float3Image _cross_normals;      // normals after cross product

  MPRPyramidLevel _top_pyramid;

  // WorkspaceEntry: contains some per level temporaries to avoid newer(s)
  struct WorkspaceEntry {
    void resize(const int rows, const int cols);
    int _rows = 0;
    int _cols = 0;
    Float3Image _direction_vectors;
    FloatImage _intensity;
    FloatImage _depth;
    Float3Image _scaled_normals;
    Float3Image _normals;
  };

  std::vector<WorkspaceEntry> _workspace;
};
}
