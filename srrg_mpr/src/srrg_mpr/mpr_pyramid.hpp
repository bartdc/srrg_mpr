namespace srrg_mpr {

bool MPRPyramidLevel::getSubPixel(Vector5f& point,
                                  Matrix5_2f& derivative,
                                  const Eigen::Vector2f& image_point) const {
  int x0 = image_point.x();  // cols
  int y0 = image_point.y();  // rows
  if (!_image.inside(y0, x0)) return false;

  int x1 = x0 + 1;
  int y1 = y0 + 1;
  if (!_image.inside(y1, x1)) return false;
  if (_mask.at<unsigned char>(y0, x0) || _mask.at<unsigned char>(y0, x1) ||
      _mask.at<unsigned char>(y1, x0) || _mask.at<unsigned char>(y1, x1))
    return false;

  const MPRPyramidImageEntry& p00 = _image(y0, x0);
  const MPRPyramidImageEntry& p01 = _image(y0, x1);
  const MPRPyramidImageEntry& p10 = _image(y1, x0);
  const MPRPyramidImageEntry& p11 = _image(y1, x1);
  const float dx = image_point.x() - (float)x0;
  const float dy = image_point.y() - (float)y0;
  const float dx1 = 1.f - dx;
  const float dy1 = 1.f - dy;

  point = (p00._point * dx1 + p01._point * dx) * dy1 +
          (p10._point * dx1 + p11._point * dx) * dy;

  derivative = (p00._derivatives * dx1 + p01._derivatives * dx) * dy1 +
               (p10._derivatives * dx1 + p11._derivatives * dx) * dy;
  return true;
}
}
