#include <srrg_mpr_ros/mpr_ros.h>

using namespace srrg_mpr;
using namespace srrg_mpr_ros;


int main(int argc, char** argv) { 

  ros::init(argc, argv, "mpr_tracker_node");
  ros::NodeHandle nh("~");
  ROS_INFO("node started");


  MPRTrackerRos* tracker = new MPRTrackerRos(nh);
  tracker->mutableConfig().depth_topic = "/camera/depth/image";
  tracker->mutableConfig().depth_camera_info = "/camera/depth/camera_info";
  tracker->mutableConfig().rgb_topic = "/camera/rgb/image_color";  
  tracker->mutableConfig().rgb_camera_info = "/camera/rgb/camera_info";

  tracker->mutableConfig().config_name = "640x480";
  
  tracker->mutableConfig().solver.omega_intensity = 1;
  tracker->mutableConfig().solver.omega_depth = 1;
  tracker->mutableConfig().solver.omega_normals = 1;
  tracker->mutableConfig().keyframe_max_translation = 0.1;
  tracker->mutableConfig().keyframe_max_rotation = 0.1;
  tracker->mutableConfig().verbose = true;
  
  tracker->init();
  
  ros::spin();
  

}
