#include "mpr_ros.h"

using namespace srrg_core;
using namespace srrg_mpr;
using namespace srrg_mpr_ros;
using namespace message_filters;


MPRTrackerRos::MPRTrackerRos(const ros::NodeHandle& nh) {
  nh_ = nh;
  config_ptr_ = new Config();
  is_initialized_ = false;
  rgb_subscriber_ = NULL;
  depth_subscriber_ = NULL;
  rgb_camera_info_subscriber_ = NULL;
  depth_camera_info_subscriber_ = NULL;
  image_sync_policy_ = NULL;
  camera_info_sync_policy_ = NULL;
  image_synchronizer_ = NULL;
  camera_info_synchronizer_ = NULL;
  setup();
}

MPRTrackerRos::~MPRTrackerRos() {
  delete config_ptr_;
  delete rgb_subscriber_;
  delete depth_subscriber_;
  delete rgb_camera_info_subscriber_;
  delete depth_camera_info_subscriber_;
  delete image_sync_policy_;
  delete camera_info_sync_policy_;  
  delete image_synchronizer_;
  delete camera_info_synchronizer_;
}

MPRTrackerRos::Config& MPRTrackerRos::mutableConfig() {
  return *config_ptr_;
}

const MPRTrackerRos::Config& MPRTrackerRos::constConfig() const {
  return *config_ptr_;
}

void MPRTrackerRos::init() {
  // todo check topic names not empty

  StringMPRTrackerConfigMap configs;
  initConfigs(configs);
  auto config_it = configs.find(config_ptr_->config_name);
  if (config_it == configs.end()) {
    cerr << "config [" << config_ptr_->config_name << "] not available" << endl;
    std::exit(0);
  }
  MPRTracker::Config& father_config = config_it->second;
  config_ptr_->pyramid.min_depth = father_config.pyramid.min_depth;
  config_ptr_->pyramid.max_depth = father_config.pyramid.max_depth;
  config_ptr_->pyramid.scales = father_config.pyramid.scales;
  config_ptr_->levels_iterations = father_config.levels_iterations;
  
  
  rgb_subscriber_   = new ImageSubscriber(nh_, config_ptr_->rgb_topic, 1);
  depth_subscriber_ = new ImageSubscriber(nh_, config_ptr_->depth_topic, 1);
  rgb_camera_info_subscriber_   = new CameraInfoSubscriber(nh_, config_ptr_->rgb_camera_info, 1);
  depth_camera_info_subscriber_ = new CameraInfoSubscriber(nh_, config_ptr_->depth_camera_info, 1);

  image_sync_policy_       = new ImageSyncPolicy(config_ptr_->queue_size);
  camera_info_sync_policy_ = new CameraInfoSyncPolicy(config_ptr_->queue_size);
  
  image_sync_policy_->setMaxIntervalDuration(ros::Duration(config_ptr_->max_interval_duration));
  camera_info_sync_policy_->setMaxIntervalDuration(ros::Duration(config_ptr_->max_interval_duration));
  
  image_synchronizer_ = new Synchronizer<ImageSyncPolicy>(ImageSyncPolicy(*image_sync_policy_), *depth_subscriber_, *rgb_subscriber_);
  camera_info_synchronizer_ = new Synchronizer<CameraInfoSyncPolicy>(CameraInfoSyncPolicy(*camera_info_sync_policy_), *depth_camera_info_subscriber_, *rgb_camera_info_subscriber_);

  image_synchronizer_->registerCallback(boost::bind(&MPRTrackerRos::callbackImages, this,  _1, _2));
  camera_info_synchronizer_->registerCallback(boost::bind(&MPRTrackerRos::callbackCamInfo, this, _1, _2));

}

void MPRTrackerRos::callbackImages(const sensor_msgs::ImageConstPtr& depth_msg,
                                   const sensor_msgs::ImageConstPtr& rgb_msg) {
  if(!is_initialized_)
    return;
  
  cv_bridge::CvImageConstPtr depth_img;
  cv_bridge::CvImageConstPtr rgb_img;
  
  try {
    depth_img = cv_bridge::toCvCopy(depth_msg, "");
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Failed to transform depth image.");
    return;
  }
  try {
    rgb_img = cv_bridge::toCvCopy(rgb_msg, "");
  }
  catch (cv_bridge::Exception& e) {
    ROS_ERROR("Failed to transform depth image.");
    return;
  }
  
  FloatImage float_image;
  if(depth_img->image.type() == CV_16UC1)
    srrg_core::convert_16UC1_to_32FC1(float_image, depth_img->image, 0.001);
  else
    float_image = depth_img->image;
  
  setImages(float_image, rgb_img->image);
  compute();
  std::cerr << "t: " << srrg_core::t2v(globalT()).transpose() << std::endl;

}

void MPRTrackerRos::callbackCamInfo(const sensor_msgs::CameraInfoConstPtr& depth_info,
                                    const sensor_msgs::CameraInfoConstPtr& rgb_info) {
  std::cerr << ".";
  if(is_initialized_)
    return;
  
  config_ptr_->pyramid.rows = depth_info->height;  
  config_ptr_->pyramid.cols = depth_info->width;
  Eigen::Matrix3f K;
  K <<  depth_info->K[0], 0,  depth_info->K[2],
    0,  depth_info->K[4],  depth_info->K[5],
    0, 0, 1;
  
  config_ptr_->pyramid.camera_matrix = K;
  
  std::cerr << "CameraInfo Set" << std::endl;
  std::cerr << "rows: " << depth_info->height << std::endl;
  std::cerr << "cols: " << depth_info->width << std::endl;
  std::cerr << "K:\n" << config_ptr_->pyramid.camera_matrix << std::endl;
  
  is_initialized_ = true;
  
}

