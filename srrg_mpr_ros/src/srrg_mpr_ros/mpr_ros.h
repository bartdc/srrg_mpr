#pragma once

#include <srrg_mpr/mpr_tracker.h>
#include <srrg_mpr/mpr_configs.hpp>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <cv_bridge/cv_bridge.h>
#include <srrg_image_utils/depth_utils.h>


namespace srrg_mpr_ros {

  typedef message_filters::Subscriber<sensor_msgs::Image> ImageSubscriber;
  typedef message_filters::Subscriber<sensor_msgs::CameraInfo> CameraInfoSubscriber;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,
    sensor_msgs::Image> ImageSyncPolicy;
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::CameraInfo,
    sensor_msgs::CameraInfo> CameraInfoSyncPolicy;
  
  class MPRTrackerRos : public srrg_mpr::MPRTracker {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    struct Config : public srrg_mpr::MPRTracker::Config {
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
      std::string config_name = "640x480";
      std::string depth_topic = "";
      std::string rgb_topic = "";
      std::string depth_camera_info = "";
      std::string rgb_camera_info = "";
      int queue_size = 1;
      float max_interval_duration = 0.04;
    };
    
    MPRTrackerRos(const ros::NodeHandle& nh);
    ~MPRTrackerRos();

    Config& mutableConfig();   
    const Config& constConfig() const;

    void init();
    
  protected:
    
    
    void callbackImages(const sensor_msgs::ImageConstPtr& depth,
                        const sensor_msgs::ImageConstPtr& rgb);
    void callbackCamInfo(const sensor_msgs::CameraInfoConstPtr& depth_info,
                         const sensor_msgs::CameraInfoConstPtr& rgb_info);    

    
  private:
    ros::NodeHandle nh_;
    Config* config_ptr_;
    bool is_initialized_;

    ImageSubscriber* rgb_subscriber_;
    ImageSubscriber* depth_subscriber_;
    CameraInfoSubscriber* rgb_camera_info_subscriber_;
    CameraInfoSubscriber* depth_camera_info_subscriber_;    
    ImageSyncPolicy* image_sync_policy_;
    CameraInfoSyncPolicy* camera_info_sync_policy_;
    message_filters::Synchronizer<ImageSyncPolicy>* image_synchronizer_;
    message_filters::Synchronizer<CameraInfoSyncPolicy>* camera_info_synchronizer_;
  };
  
}
